# Import modules
import math
import os
import networkx as NX
import re
import copy as cp
import matplotlib.pyplot as plt

# Get oligomerization data
#Runs = 5
#Runs = [0,3]
Runs = [1,2,3]
Monomers = 6
Length1= 500
Length = 50
Cutoff = 50
OligoStates = [[[0 for z in range(Monomers)] for x in range(Length*200 + 2)] for y in range(len(Runs))]
Betas = [[[0 for z in range(Monomers)] for x in range(Length*200 + 2)] for y in range(len(Runs))]
Betas2 = [[[0 for z in range(Monomers)] for x in range(Length*200 + 2)] for y in range(len(Runs))]
CoarseBetas = [[[0 for z in range(Monomers)] for x in range(Length*200 + 2)] for y in range(len(Runs))]
MolDir = '/u2/data/suman/Analysis-paper/hexamer/a99-disp/wt/'
GraphName = 'wt-a99disp.graphml'
#StableCutOff = 20 #Snapshots = 0.1 ns 

# Get oligomerization groups
for n,i in enumerate(Runs):
	name = MolDir + 'r' + str(i) + '/oligo-groups.dat'
	file = open(name,'r')
	line = file.readline()
	j = 0
	while line:
		temp = line.split('\t')
		for k in range(Monomers):
			OligoStates[n][j][k] = temp[k + 1][1:-1].split(',')	
		j += 1
		line = file.readline()
	file.close()

# Get betas
for n,i in enumerate(Runs):
        name = MolDir + 'r' + str(i) + '/betas.dat'
        file = open(name,'r')
        line = file.readline()
        j = 0
        while line:
                temp = line.split('\t')
                for k in range(Monomers):
                        Betas[n][j][k] = float(temp[k])
                j += 1
                line = file.readline()
        file.close()
for n1,i in enumerate(Betas):
	for n2,j in enumerate(i):
		for n3,k in enumerate(j):
			for oligo in OligoStates[n1][n2][n3]:	
                            Betas2[n1][n2][n3] += Betas[n1][n2][int(float(oligo))]
			Betas2[n1][n2][n3] /= len(OligoStates[n1][n2][n3])

# Coarse grain the betas value (between 0.0 and 0.2 == 1, between 0.2 and 0.4 == 2,etc)
for n1,i in enumerate(Betas2):
	for n2,j in enumerate(i):
		for n3,k in enumerate(j):
			if (k < 0.2):
				CoarseBetas[n1][n2][n3] = 1
			elif k > 0.2 and k < 0.4:
				CoarseBetas[n1][n2][n3] = 2
			elif k > 0.4 and k < 0.6:
				CoarseBetas[n1][n2][n3] = 3
			elif k > 0.6 and k < 0.8:
				CoarseBetas[n1][n2][n3] = 4
			else:
				CoarseBetas[n1][n2][n3] = 5
# Create empty graph
CoarseGraph = NX.DiGraph()

for n1,i in enumerate(OligoStates):
	for n2,j in enumerate(i[::Cutoff]):
		for n3,k in enumerate(j):
			# Calculate the node name			
			name = str(len(k)) + '-' + str(CoarseBetas[n1][n2*Cutoff][n3])
			CoarseGraph.add_node(name)

# Create count matrix (It is a dictionary really)
CountMatrix = {}
for x in CoarseGraph.nodes():
	for y in CoarseGraph.nodes():
		CountMatrix[x,y] = 0

# Create concentration dictionary
Concentration = {}
for x in CoarseGraph.nodes():
	Concentration[x] = 0

# Temporary state lists
StatesOld = [0. for x in range(Monomers)]
StatesNew = [0. for x in range(Monomers)]

# Calculate transitions and concentration of microstates
for n2,i in enumerate(OligoStates):
	first = 1
	for n1,j in enumerate(i[::Cutoff]):
		if first == 0:
			for n,k in enumerate(j):
				name = str(len(k)) + '-' + str(CoarseBetas[n2][n1*Cutoff][n])
				StatesNew[n] = name	
			# Calculate concentration
			for k in StatesNew:
				Concentration[k] += 1

			# Check for transitions
			for n,k in enumerate(StatesNew):
				if (k != StatesOld[n]):
					CountMatrix[StatesOld[n],k] += 1 		
			StatesOld = cp.deepcopy(StatesNew)
		# First snapshot
		else:
			for n,k in enumerate(j):
				name = str(len(k)) + '-' + str(CoarseBetas[n2][n1*Cutoff][n])
				StatesOld[n] = name
			# Calculate concentration
			for k in StatesOld:
				Concentration[k] += 1
			first = 0    

print (Concentration)

# Add data as attributes to nodes and edges
for x in CoarseGraph.nodes():
	CoarseGraph.node[x]['concentration'] = Concentration[x]
	state = x.split('-')
	CoarseGraph.node[x]['state'] = int(float(state[0]))
	CoarseGraph.node[x]['betas'] = int(float(state[1]))

for x in CoarseGraph.nodes():
	for y in CoarseGraph.nodes():
		if ((x != y) and ( CountMatrix[x,y] != 0)):
			CoarseGraph.add_edge(x,y,weight = CountMatrix[x,y])
   
NX.write_graphml(CoarseGraph,GraphName)
