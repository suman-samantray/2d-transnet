import copy as cp
import subprocess as sp
import os as os
import shutil as sh
import sys
import MDAnalysis as mdana

r = sys.argv[1]
length = 50
atoms = 134

residues = 7
monomers = 6

# Create lists
betas = [[0 for x in range(monomers)] for x in range(int(length)*200 + 2)]


proteingroups = []
phiatoms = [[] for x in range(monomers)]
psiatoms = [[] for x in range(monomers)]

# Calculate the secondary structure for each of the corresponding replicas
dir = '/u2/data/suman/Analysis-paper/hexamer/a99-disp/wt/r'+ r
os.chdir(dir)

# Create Universe 
uni = mdana.Universe('prod-a99disp.pdb','prod-a99disp-nopbc.xtc')

for m in range(0,monomers):
	for res in range(2,residues+1):
		phiatoms[m].extend([ uni.select_atoms('bynum '+str(1 + atoms*m)+':'+str(atoms*(m+1))+' and (resid ' + str(res-1) + ' and name C or resid ' + str(res)+ ' and ( name N or name CA or name C ))')])
		psiatoms[m].extend([ uni.select_atoms('bynum '+str(1 + atoms*m)+':'+str(atoms*(m+1))+' and ((resid ' + str(res) + ' and ( name N or name CA or name C )) or (resid ' + str(res+1)+ ' and name N ))')])

for i1,t in enumerate(uni.trajectory):
#    print(t)
# Calculate dimension of the box to considerd PBC
    box = t.dimensions[:3]
    for m in range(monomers):
        for n in range(residues-1):
            if psiatoms[m][n].dihedral.value() > 90 and psiatoms[m][n].dihedral.value() < 180 and phiatoms[m][n].dihedral.value() > -180 and phiatoms[m][n].dihedral.value() < -50:
                if psiatoms[m][n].dihedral.value() > 108 and psiatoms[m][n].dihedral.value() < 126 and phiatoms[m][n].dihedral.value() > -162 and phiatoms[m][n].dihedral.value() < -144:
                    betas[i1][m] += 1./6
                if not ( psiatoms[m][n].dihedral.value() > 90 and psiatoms[m][n].dihedral.value() < 126 and phiatoms[m][n].dihedral.value() > -180 and phiatoms[m][n].dihedral.value() < -144):
                    betas[i1][m] += 1./6
# Save data

fileout = open ('betas.dat','w')
for n,i in enumerate(betas):
        for j in i:
                fileout.write(str(j)+'\t')
        fileout.write ('\n')
fileout.close()
